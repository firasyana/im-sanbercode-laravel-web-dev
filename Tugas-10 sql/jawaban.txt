Jawaban:
1. Membuat Database  
CREATE DATABASE myshop;

2. Membuat Table di Dalam Database
a. users
create table users(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    primary key(id)
);

b. categories
create table categories(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    primary key(id)
);

c. items
 create table items(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    primary key(id),
    foreign key(category_id) REFERENCES categories(id)
);

3. Memasukkan Data pada Table
a. users
insert into users(name, email, password)
    values('John Doe', 'john@doe.com', 'john123'),
    ('Jane Doe', 'jane@doe.com', 'jenita123');

b. categories
insert into categories(name)
    values('gadget'), ('cloth'), ('men'), ('women'), ('branded');

c. items
insert into items(name, description, price, stock, category_id)
    value('Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1'),
    ('uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'),
    ('IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');

4. Mengambil Data dari Database
a. Mengambil data users
select id, name, email from users;

b. Mengambil data items
- select * from items where price > 1000000;
- select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori
select items.*, categories.name as kategori from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari Database
update items set price = 2500000 where name = 'Sumsang b50';