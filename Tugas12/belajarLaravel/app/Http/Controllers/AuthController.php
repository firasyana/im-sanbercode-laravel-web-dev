<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('signup.register');
    }

    public function welcome(Request $request)
    {
        $firstName = $request->input('first-name');
        $lastName = $request->input('last-name');
        $gender =  $request->input('gender');
        $nationality =  $request->input('nationality');
        $languageSpoken =  $request->input('language-spoken');

        return view('signup.welcome', ["firstName" => $firstName, "lastName" => $lastName]);
    }
}
