<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:5',
        ]);
        DB::table('cast')->insert([
            "nama" => $request->input("nama"),
            "umur" => $request->input("umur"),
            "bio" => $request->input("bio"),
        ]);
        return redirect('/cast');
    }
    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', ['cast' => $cast]);
    }
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', ['cast' => $cast]);
    }
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|min:5',
        ]);
        DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request->input("nama"),
            "umur" => $request->input("umur"),
            "bio" => $request->input("bio"),
            ]);
        return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
