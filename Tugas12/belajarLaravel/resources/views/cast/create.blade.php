@extends('layouts.master')
@section('title')
    Halaman Create Cast
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="nama">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control @error('bio') is-invalid @enderror" name="bio"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection