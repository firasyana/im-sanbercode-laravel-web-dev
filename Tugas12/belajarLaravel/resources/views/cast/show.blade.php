@extends('layouts.master')
@section('title')
    Halaman Show Cast
@endsection

@section('content')

<h2>Nama: {{$cast->nama}}</h2>
<h4>Umur: {{$cast->umur}}</h4>
<p>Bio: {{$cast->bio}}</p>

<a href="/cast" class="btn btn-primary btn-sm my-3">Back</a>
@endsection
