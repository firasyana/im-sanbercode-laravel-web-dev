@extends('layouts.master')
@section('title')
    Form Page
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="first-name"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last-name"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language-spoken">Bahasa Indonesia<br>
        <input type="checkbox" name="language-spoken">English<br>
        <input type="checkbox" name="language-spoken">Other<br><br>
        <label>Bio:</label><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
@endsection