<?php
require('Animal.php');
require('Frog.php');
require('Ape.php');

echo "<h3>Release 0</h3>";

$sheep = new Animal("shaun");
echo "name: " . $sheep->name . "<br>"; // "shaun"
echo "legs: " . $sheep->get_legs() . "<br>"; // 4
echo "cold_blooded: " . $sheep->get_cold_blooded() . "<br>"; // "no"

echo "<h3>Release 1</h3>";

$kodok = new Frog("buduk");
echo "name: " . $kodok->name . "<br>"; // "buduk"
echo "legs: " . $kodok->get_legs() . "<br>"; // 4
echo "cold_blooded: " . $kodok->get_cold_blooded() . "<br>"; // "no"
$kodok->jump(); // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "name: " . $sungokong->name . "<br>"; // "kera sakti"
echo "legs: " . $sungokong->get_legs() . "<br>"; // 2
echo "cold_blooded: " . $sungokong->get_cold_blooded() . "<br>"; // "no"
$sungokong->yell() // "Auooo"
?>