<?php

class Animal
{
    // properti
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    //contructor
    public function __construct($name)
    {
        $this->name = $name;
    }

    //methods
    public function set_legs($legs){
        $this->legs = $legs;
    }
    public function get_legs(){
        return $this->legs;
    }

    public function set_cold_blooded($cold_blooded){
        $this->legs = $cold_blooded;
    }
    public function get_cold_blooded(){
        return $this->cold_blooded;
    }


}
?>